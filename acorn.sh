#!/bin/bash

#-----------------------------------------------------------
#----------------------  Help Screen  ----------------------
#-----------------------------------------------------------
show_usage() {
  echo "   _____"
  echo "  /  _  \   ____  ___________  ____  "
  echo " /  /_\  \_/ ___\/  _ \_  __ \/    \ "
  echo "/    |    \  \__(  <_> )  | \/   |  \\"
  echo "\____|__  /\___  >____/|__|  |___|  /"
  echo "        \/     \/                 \/ "
  echo ""
  echo "Acorn is a self contained environment setup and configuration script for building out docker swarm clusters."
  echo ""
  echo "Usage: $0 [options]"
  echo "  - $0 - displays this screen"
  echo "  - -h|--help - displays this screen"
  echo "Core Options"
  echo "  - --create - set up docker and create a swarm"
  echo "  - --join [token] [manager-ip] - set up docker and join an existing swarm"
  echo "  - --add-label [label-name] [node-name] - add label to the node"
  echo "  - --rm-label [label-name] [node-name] - delete label from node"
  echo "  - --deploy-core - deploys the core platform including portainer and traefik"
  echo "  - --deploy-monitoring - deploys a monitoring platform including grafana and prometheus"
  echo "  - --deploy-alert-monitoring - deploys a monitoring platform including grafana, prometheus and alertmanager"
  echo "  - --enable-ssl-termination - updates the configuration to enable ssl on traffic passing through traefik"
  echo "  - --disable-ssl-termination - updates the configuration to disable ssl on traffic passing through traefik"
  echo "  - --expose-ingress-tools - updates the configuration to route traffic through the ingress controller"
  echo "  - --disable-ingress-tools - updates the configuration to disable routing traffic through the ingress controller"
  echo "  - --enable-loki-integration - updates the configuration to send container and system logs to loki"
  echo "  - --disable-loki-integration - updates the configuration to disable send container and system logs to loki"

  echo "  - --upgrade - applies the latest configuration to an existing environment"

  #-----------------------------------------------------------
  #------------------  Custom Help Screen  -------------------
  #-----------------------------------------------------------

  # echo "Custom Options"
  # echo "  - --custom - set up a custom process"

  #-----------------------------------------------------------
  #---------------  End of Custom Help Screen  ---------------
  #-----------------------------------------------------------

  echo " Example"
  echo "  - Deploy core and monitoring: $0 --enable-ssl-termination --deploy-core --deploy-monitoring"

  exit 1
}

if [ -z "$1" ]; then
  show_usage
  exit 1
fi

if [ $EUID != 0 ]; then
    sudo "$0" "$@"
    exit $?
fi

DOMAIN=local
COMPOSE_FILES=( 
  "./core/docker-compose.yml" 
  "./core/docker-compose-portainer.yml" 
  "./monitoring/docker-compose.yml" 
  "./monitoring/docker-compose-alertmanager.yml"
)

# Import environmental variables from .env
if [ -f ".env" ]; then
  export $(egrep -v '^#' .env | xargs) > /dev/null
fi

case "$(uname -m)" in
  x86_64)
    ARCH_TYPE="amd64"
    ;;
  arm64)
    ARCH_TYPE="arm64"
    ;;
  arm)
    ARCH_TYPE="armhf"
    ;;
  *)
    ARCH_TYPE="unknown"
esac

sed_compose_files() {
  for i in "${COMPOSE_FILES[@]}"
  do
    sed -i "$1" $i
  done
}

prepare_debian_environment() {
  echo "Debian environment detected, proceeding with environment configuration."

  apt-get update

  apt-get dist-upgrade -y

  apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common

  curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -

  add-apt-repository \
    "deb [arch=${ARCH_TYPE}] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) \
    stable"

  apt-get update

  apt-get install -y docker-ce docker-ce-cli docker-compose containerd.io

  #-----------------------------------------------------------
  #--------------  Custom Prepare Environment  ---------------
  #-----------------------------------------------------------

  # apt-get install -y node

  #-----------------------------------------------------------
  #-----------  End of Custom Prepare Environment  -----------
  #-----------------------------------------------------------

  if [ ! -f "/etc/docker/daemon.json" ]; then
    echo "Creating docker daemon configuration and restart docker..."
    echo '{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2",
  "metrics-addr" : "0.0.0.0:9323",
  "experimental" : true
}' > /etc/docker/daemon.json

    service docker restart

    echo "Waiting for docker to start back up before continuing..."
    sleep 5
  fi
}

prepare_environment() {
  case "$OSTYPE" in
    linux-gnu)
      prepare_debian_environment
      ;;
    darwin)
      echo "MacOSX environment detected, skipping environment preparations"
      ;;
    *)
      echo "Unknown environment, skipping environment preparations"
      ;;
  esac
}

deploy_core() {
  echo "Creating networks (it is normal for them to error if they already exist)"
  docker network create -d overlay traefik-public
  docker network create -d overlay agent-network

  echo "Deploying core stack..."
  docker stack deploy -c core/docker-compose.yml core
  docker stack deploy -c core/docker-compose-portainer.yml portainer

  sed -i "s/^.*# acorn-core-dns$//g" /etc/hosts
  sed -i '/^$/d' /etc/hosts

  echo "127.0.0.1        portainer.${DOMAIN} traefik.${DOMAIN} # acorn-core-dns" >> /etc/hosts
}

deploy_monitoring() {
  echo "Creating networks (it is normal for them to error if they already exist)"
  docker network create -d overlay monitoring

  # Reset the prometheus configuration to default
  sed -i "s/^([^#]+)# alert-monitoring$/#\1# alert-monitoring/g" ./monitoring/prometheus/prometheus.yml

  echo "Deploying monitoring stack..."
  if [ -z "$1" ]; then
    docker stack deploy -c monitoring/docker-compose.yml monitoring
  else
    while [ ! -z "$1" ];do
      case "$1" in
        alert)
          # Uncomment #alert-monitoring in prometheus.yml  
          sed -i "s/^#(.*)# alert-monitoring$/\1# alert-monitoring/g" ./monitoring/prometheus/prometheus.yml
          docker stack deploy -c monitoring/docker-compose-alertmanager.yml alert-monitoring
          ;;
        default)
          docker stack deploy -c monitoring/docker-compose.yml monitoring
          ;;
      esac
      shift
    done
  fi

  sed -i "s/^.*# acorn-monitoring-dns$//g" /etc/hosts
  sed -i '/^$/d' /etc/hosts

  echo "127.0.0.1        grafana.${DOMAIN} alertmanager.${DOMAIN} prometheus.${DOMAIN} # acorn-monitoring-dns" >> /etc/hosts
}

#-----------------------------------------------------------
#--------------  Custom Function Definition  ---------------
#-----------------------------------------------------------
        
# custom_function() {
#   echo "do something"
# }

#-----------------------------------------------------------
#-----------  End of Custom Function Definition  -----------
#-----------------------------------------------------------

#-----------------------------------------------------------
#------------------  Main Function Loop  -------------------
#-----------------------------------------------------------

while [ ! -z "$1" ];do
  case "$1" in
    -h|--help)
      show_usage
      ;;
    --create)
      prepare_environment
      docker swarm init
      deploy_core
      ;;
    --join)
      shift
      TOKEN="$1"
      shift
      MANAGER_IP="$1"
      prepare_environment
      docker swarm join --token ${TOKEN} ${MANAGER_IP}
      ;;
    --domain)
      shift
      DOMAIN="$1"
      ;;
    --add-label)
      shift
      LABEL="$1"
      shift
      NODE_NAME="$1"
      docker node update --label-add ${LABEL} ${NODE_NAME}
      ;;
    --rm-label)
      shift
      LABEL="$1"
      shift
      NODE_NAME="$1"
      docker node update --label-rm ${LABEL} ${NODE_NAME}
      ;;
    --deploy-core)
      deploy_core
      ;;
    --deploy-monitoring)
      deploy_monitoring
      ;;
    --deploy-alert-monitoring)
      deploy_monitoring alert default
      ;;
    --enable-ssl-termination)
      sed_compose_files "s/#\(.*\) # letsencrypt-ssl-termination/\1 # letsencrypt-ssl-termination/g"
      echo "Changes completed. Re-deploy service stacks to apply changes."
      ;;
    --disable-ssl-termination)
      sed_compose_files "s/^\([^#]*\) # letsencrypt-ssl-termination/#\1 # letsencrypt-ssl-termination/g"
      echo "Changes completed. Re-deploy service stacks to apply changes."
      ;;
    --expose-ingress-tools)
      sed_compose_files "s/#\(.*\) # expose-ingress-tool/\1 # expose-ingress-tool/g"
      echo "Changes completed. Re-deploy service stacks to apply changes."
      ;;
    --disable-ingress-tools)
      sed_compose_files "s/^\([^#]*\) # expose-ingress-tool/#\1 # expose-ingress-tool/g"
      echo "Changes completed. Re-deploy service stacks to apply changes."
      ;;
    --enable-loki-integration)
      echo "Attempting to install loki-docker-driver (it is normal to error if it's already installed)"
      case "$(uname -m)" in
        arm64)
          docker plugin install grafana/loki-docker-driver:arm-64 --alias loki --grant-all-permissions
          ;;
        *)
          docker plugin install grafana/loki-docker-driver --alias loki --grant-all-permissions
          ;;
      esac

      echo "Applying changes to docker daemon"

      sed -i "s/\"log-driver\": \"json-file\",/\"log-driver\": \"loki\",/g" /etc/docker/daemon.json
      sed -i "s/\"max-size\": \"100m\"/\"loki-url\": \"http:\/\/localhost:3100\/loki\/api\/v1\/push\"/g" /etc/docker/daemon.json

      service docker restart

      sed_compose_files "s/#\(.*\) # loki-integration/\1 # loki-integration/g"
      
      echo "Changes completed. Logs are now being sent to localhost:3100.  To change this, edit /etc/docker/daemon.json Re-deploy service stacks to apply changes."
      
      ;;
    --disable-loki-integration)
      echo "Applying changes to docker daemon"

      sed -i "s/\"log-driver\": \"loki\",/\"log-driver\": \"json-file\",/g" /etc/docker/daemon.json
      sed -i "s/\"loki-url\": \".*\"/\"max-size\": \"100m\"/g" /etc/docker/daemon.json

      service docker restart

      sed_compose_files "s/^\([^#]*\) # loki-integration/#\1 # loki-integration/g"

      echo "Changes completed. Re-deploy service stacks to apply changes."
      ;;
    -u|--upgrade)
      prepare_environment
      ;;

    #-----------------------------------------------------------
    #---------------  Custom Option Definition  ----------------
    #-----------------------------------------------------------

    # --custom)
    #   prepare_environment
    #   ;;

    #-----------------------------------------------------------
    #------------  End of Custom Option Definition  ------------
    #-----------------------------------------------------------
    *)
    echo "Unknown or invalid command option $1"
    show_usage
  esac
shift
done