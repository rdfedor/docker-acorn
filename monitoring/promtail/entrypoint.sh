#!/bin/sh -e

cp /etc/promtail/docker-config.yml /etc/promtail/config.yml

sed -i "s/host: localhost/host: $(cat /etc/nodename)/g" /etc/promtail/config.yml

set -- /usr/bin/promtail "$@"

exec "$@"