
## Command Line Interface

```
   _____
  /  _  \   ____  ___________  ____  
 /  /_\  \_/ ___\/  _ \_  __ \/    \ 
/    |    \  \__(  <_> )  | \/   |  \
\____|__  /\___  >____/|__|  |___|  /
        \/     \/                 \/ 

Acorn is a self contained environment setup and configuration script for building out docker swarm clusters.

Usage: ./acorn.sh [options]
  - ./acorn.sh - displays this screen
  - -h|--help - displays this screen
Core Options
  - --create - set up docker and create a swarm
  - --join [token] [manager-ip] - set up docker and join an existing swarm
  - --add-label [label-name] [node-name] - add label to the node
  - --rm-label [label-name] [node-name] - delete label from node
  - --deploy-core - deploys the core platform including portainer and traefik
  - --deploy-monitoring - deploys a monitoring platform including grafana and prometheus
  - --deploy-alert-monitoring - deploys a monitoring platform including grafana, prometheus and alertmanager
  - --enable-ssl-termination - updates the configuration to enable ssl on traffic passing through traefik
  - --disable-ssl-termination - updates the configuration to disable ssl on traffic passing through traefik
  - --expose-ingress-tools - updates the configuration to route traffic through the ingress controller
  - --disable-ingress-tools - updates the configuration to disable routing traffic through the ingress controller
  - --enable-loki-integration - updates the configuration to send container and system logs to loki
  - --disable-loki-integration - updates the configuration to disable send container and system logs to loki
  - --upgrade - applies the latest configuration to an existing environment
 Example
  - Deploy core and monitoring: ./acorn.sh --enable-ssl-termination --deploy-core --deploy-monitoring
```

## Infrastructure Services

Acorn is meant to provide a minimal platform as a foundation to automate the process of provisioning an environment from scratch to create a Docker Swarm cluster.  The bash script automates the process for debian based systems to add Docker's debian repository to the host, install & configure docker, upgrades the environment over time and provides a way to version control infrastructure changes. 

### Example Setups

#### One Node Cluster

Even as a single node Docker Swarm cluster, it provides more control over the resources that containers consume compared to a standalone docker instance.  More budget minded projects can be logically designed for growth by using constraints to deploy services to nodes with specific labels but just have all the labels on a single node cluster.  This gives the opportunity down the road to add nodes to the cluster as budget becomes available to migrate and scale the services.

#### Two Manager Node Cluster

A two node cluster, both configured as a managers, can provide load balancing and redundancy for services where uptime is critical. Cluster management software is distributed across both nodes.  To provide a common place for volume storage, a nfs storage location can be mounted to both nodes but that can be added later.  Same benefits would apply from a One Node Cluster where as resources become available, additional nodes can be added to easily migrate and scale services.

### Services

#### Traefik - Cluster Ingress Controller

Traefik offers a reverse proxy that helps route traffic to the appropriate services within the cluster. When deploy-core-ssl is used it also acts as an ssl termination server w/ automatic certificate generation through LetsEncrypt.  It uses compose deploy labels to help detect how to configure Traefik to route connections to the service.

Usage: deploy-core / deploy-core-ssl

Network: traefik-public

[Homepage](https://traefik.io/) | [Docker Hub](https://hub.docker.com/_/traefik) | [Documentation](https://doc.traefik.io/traefik/v2.0/)

#### Portainer-CE

Portainer is an administrative dashboard for managing clusters for Kubernetes, Docker, Docker Swarm and Azure ACI. It can help with managing the various aspects of running a cluster like managing images, containers, networks, logs and more.

Usage: deploy-core / deploy-core-ssl

Network: agent-network

[Homepage](https://www.portainer.io/) | [Docker Hub](https://hub.docker.com/r/portainer/portainer-ce) | [Documentation](https://documentation.portainer.io/)

#### Portainer-CE Agent

Helps portainer collect information from the cluster.

Usage: deploy-core / deploy-core-ssl

Network: agent-network

[Homepage](https://www.portainer.io/) | [Docker Hub](https://hub.docker.com/r/portainer/agent) | [Documentation](https://documentation.portainer.io/archive/1.23.2/agent/)

#### Grafana

Provides various visual dashboards of the data collected by prometheus on cluster performance that can help in diagnosing problems.

Usage: deploy-monitoring / deploy-alert-monitoring

Network: monitoring

[Homepage](https://grafana.com/) | [Docker Hub](https://registry.hub.docker.com/r/grafana/grafana) | [Documentation](https://grafana.com/docs/grafana/latest/installation/docker/)

#### Docker-Exporter / Caddy

A simple reverse proxy using Caddy which exposes docker's built in metrics to the monitoring network for Prometheus to collect.

Usage: deploy-monitoring / deploy-alert-monitoring

Network: monitoring

[Homepage](https://caddyserver.com/) | [Docker Hub](https://hub.docker.com/_/caddy) | [Documentation](https://caddyserver.com/docs/) | [Docker Prometheus Metrics](https://docs.docker.com/config/daemon/prometheus/)

#### CAdvisor (deploy-monitoring / deploy-alert-monitoring)

cAdvisor (Container Advisor) provides container users an understanding of the resource usage and performance characteristics of their running containers. It is a running daemon that collects, aggregates, processes, and exports information about running containers. Specifically, for each container it keeps resource isolation parameters, historical resource usage, histograms of complete historical resource usage and network statistics. This data is exported by container and machine-wide.

Usage: deploy-monitoring / deploy-alert-monitoring

Network: monitoring

[Homepage](https://github.com/google/cadvisor) | [Docker Hub](https://hub.docker.com/r/google/cadvisor)

#### Node Exporter

Prometheus exporter for hardware and OS metrics exposed by *NIX kernels, written in Go with pluggable metric collectors.

Usage: deploy-monitoring / deploy-alert-monitoring

Network: monitoring

[Homepage](https://github.com/prometheus/node_exporter) | [Docker Hub](https://hub.docker.com/r/prom/node-exporter)

#### Alertmanager

Alertmanager handles alerts sent by client applications such as the Prometheus server. It takes care of deduplicating, grouping, and routing them to the correct receiver integration such as email, PagerDuty, or OpsGenie. It also takes care of silencing and inhibition of alerts.

Usage: deploy-alert-monitoring

Network: monitoring

[Homepage](https://github.com/prometheus/alertmanager) | [Docker Hub](https://hub.docker.com/r/prom/alertmanager) | [Documentation](https://prometheus.io/docs/alerting/latest/alertmanager/)

#### Karma

Karma is a dashboard for alertmanager for browsing alerts and managing silences.  Duplicated alerts are deduplicated so only unique alerts are displayed. Each alert is tagged with names of all Alertmanager instances it was found at and can be filtered based on those tags.

Usage: deploy-alert-monitoring

Network: monitoring

[Homepage](https://karma-dashboard.io/) | [Docker Hub](https://hub.docker.com/r/lmierzwa/karma) | [Documentation](https://github.com/prymitive/karma)

### Networks

| Name           | Public       | Description  |
| :------------- | :----------: | :----------- |
| traefik-public | Yes          | The public network where the reverse proxy will route incoming requests from HTTP (or HTTPS). |
| agent-network  | No           | Network where all the portainer related services run on. |
| monitoring     | No           | Network for cluster monitoring services to communicate with prometheus and grafana. |

### Port Map

| Port           | Node Role    | Service      | Description  |
| :------------: | :----------: | :----------: | :----------- |
| 80/TCP         | Manager      | Traefik      | Routes non-http traffic to the appropriate services on the traefik-public network. If SSL is enabled, routes all traffic to https. |
| 443/TCP        | Manager      | Traefik      | SSL termination and routes traffic to the appropriate services on the traefik-public network.  Only enabled if core-ssl deployed. |
| 3000/TCP       | Manager      | Grafana      | Routes to Grafana dashboard. |
| 3100/TCP       | Manager      | Loki         | Loki endpoint for collecting logs |

| 8000/TCP       | Manager      | Portainer    | Reverse proxy used by protainer for edge server management. |
| 8080/TCP       | Manager      | Traefik      | Traefik system monitor dashboards. |
| 8081/TCP       | Manager      | Karma        | Dashboard for AlertManager. |
| 9000/TCP       | Manager      | Portainer    | Administrative dashboard for cluster management. |
| 9090/TCP       | Manager      | Prometheus   | Routes to the Prometheus PromQL dashboard. |
| 9093/TCP       | Manager      | AlertManager | Routes to AlertManager dashboard. |
